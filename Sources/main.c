//				%% This code is a simple code with PWM off time in start followed by ON time.
// 				   It has only a rampup and blind start.
//				% 
//				% USAGE:
//				%
//				% INPUT:
//				%
//				% OUTPUT:
//				%
//				% DEPENDENCIES: esc07092014 project
//				% 
//				% NOTES:
//				%
//				% EXAMPLE:
//				%
//				% Source: C:\Users\Praveena\Dropbox\Ziphius-revised uC and GPS\Software\workspace\esc10062014\Sources\main.c
//				% Author: Vinod Karuvat
//				% Version: 1.4; Revision: 1.0
//				% History Table: 
//				%				1.0 - implemented a PWM off init followed by PWM on. TPM1 prescaler changed to 16 from 32.
//				%				1.1 - with the changes done in 1.0 the 50uSecs(PWM) became 100uSecs.
//				%				1.2 - reverted back to old Filipe code and got a better waveform (with '0' crossing)
//				%				    - removed the rampUp code section.
//				%					- adcread added.
//				%      				- adc is made 10 bit from previous 12 bit.
//              %					- 1000uF capacitors were removed and caused a lot of issues (waveform corruption) !!!!!!!!!!!!
//				%					- When 1000uF were added back it was more smooth.
//				%					- TPM1 prescaler is changed from 32 to 16.
//				%					- ADC reading is becoming a bit of a pain !!!!!!
//				%					- date - 21st July - it is now detecting correct '0' crossing. WELL DONE !!!!!!!!
//				%										 sometimes it crashes after '0' cross detection.
//				%					- 					 Implement a restart if '0' cross not detected properly :)
//				%					- date - 17th Sept - restarted coding. Following to be implemented -
//				%										 1) Incremement duty cycle and theryby monitor an increment in thrust.
//				%										 2) Arduino interfacing to be achieved.
//              %										 3) Current measurement.
//              %										 4) Reverse motion.
//              %										 5) Voltage of battery.
//              %				1.3 - date - 1st Oct  -  ISSUES !!!!!! -
//              %										 1) It was not detecting '0' crossing properly. 
//              %										 2) The testing range (adc) is now 120 to 145(in hex).
//              %										 3) What happens is it detects '0' crossing, then looses it, regains it and looses,...etc
//              %										 4) '0' crossing is being done only once. So, it makes sense that it looses it in corresponding iterations.
//              %										 Next steps - Implement a dynamic '0' crossing.
//              %				1.4 - date - 7th Oct  - 
//              %										 1) Implement restart on stall :)
//              %										 2) Works till abt 40%. At 40% it stalls automatically and restarts. LOOK INTO THIS !!!!!!!!!!!!!!!!
//              %										 3) Not sure if "RESTART" is the word. Just goes haywire !!!!!!
//              %										 4) Stall overcome is done as well. Works quite well.						 
//              %				1.5 - date - 2nd Jan - 2015 -
//              %										 1) Implement a LPTMR for input from arduino.
//              %										 2) Make '0' crossing even more robust !!!!!!!!!
//              %				1.6 - date - 9th Mar - 2015 - 
//              %										 1) Made function of sspeed change (the blind startup used to do all). Now used blind startup to call a function that does that.
//              %											Can be used as a general function as well.
//              %										 2) NOTE - Emulated the PWM from Arduino in software. It works like 65% of the times.
//              %				
//              %  				1.7 - date - 11th Mar - 2015 - 
//              %										 1) It starts and goes good with tempCount = 6000.
//              %								
//              %				1.8 - date - 18th March - 2015 - 
//              %										 1) Write a software filter to remove false +ves of initial '0' crossing detection.
//              %				
//              %				
//              %				
//              %				 /// ~~~~ BUGS -    1) Sometimes fails to catch the 1st '0' crossing. ~~~~ //
//              %
//              %  //-----------------   Edit history ------------------------//
//              %  02182015 - 1) Start the LPTMR implementation.
//              %
//              %   NOTE - THIS VERSION HAS THE FOLLOWING FEATURES 
//              %          1) It works without much stall.
//              %          2) Duty cycle varied from 10 to 40 and no stall detected.
//              %          3) Test more.
//				%		   4) The final aspect is the interface with PLucas Arduino. This is achieved via the LPTMR !!!!!!!
//				%			  
//              %          Parameters - duty cycle tested - 10 to 40%
//              %                       ADC val - 0x120 to 0x145.
//              %   
//              %   
// 				%			Tests - 
//				%					1) For 10% duty cycle, low PWM first and 6 states with final tempCount of 1800 consumes - 0.5887Amps.
//				%					with high PWM first also it consumes - 0.587Amps. But current consumption reduces with increase in frequency (instead
//				%					of 50uSecs make it 100uSecs.
//				%					When made 100uSecs it reduces power consumption. But speed reduces drastivally and also there is more step like 
//				%					BEMF in the 2nd half of the PWM off time. THIS TEST IS OBSOLETE !!!!!!!!!!
//				%
//				%			Note :- 1) SOMEHOW INTERRUPTS DONT WORK ON THE NEW BOARDS (2nd batch of green boards) !!!!!!!!!!!!!!!!!!!!!
//				%
//				%   +------------------------------------------------+
//				%   |           Azorean								 |
//				%   |       									     |
//				%   +------------------------------------------------+


// ---------------- Variables, functions defined ------------- //
                #include "derivative.h" /* include peripheral declarations */
                #include "math.h"
                #include "stdio.h"
                
                static interruptsInit(void);
                void FTM0_IRQHandler(void);
                void FTM1_IRQHandler(void);                
                
                static void gpioInit(void);
                static void pwmInit(void);
                static void commutationTimerInit(void);
                static void stateChange(uint8_t nextState,uint8_t dutyCycle);
                static void commTimerFunc(uint16_t commutationTimeValue);                
                static void prePosition(void);				
                static void setPWMDutyCycle(uint16_t dutyCycle,uint8_t channel);
                static void commutationAction(uint8_t state,uint16_t periodRamp,int dutyCycle);
                static void zeroCrossFind(uint8_t iterationNumber);
                static void adcFormat(uint8_t adcCount,uint8_t timeBase);
                static void adcInit(void);
                static void pitInit(void);
                static void pitStart(int pitTout);
                static void adcConfig(void);                
                
                static uint8_t rampUp(uint8_t commState); 
                static uint8_t zeroCrossTimeDetect(void);
                static uint8_t uartConfig(void);
                static uint8_t uartDisp(uint8_t dispData);                
                static uint8_t adcCalib(void);
                static uint8_t runMode(uint8_t commState);
                static uint8_t rampUp(uint8_t commState);
				static uint8_t blindStartUp(void);                
                static uint8_t zeroCrossDetect(uint16_t adcValue);
                static uint8_t stallDetectFunction(uint16_t adcValPres, uint16_t adcValPrev);
                static uint8_t restartFlag;                
				static uint8_t speedVaryDutyCycle(uint8_t startDutyCycle,uint8_t finalDutyCycle,int tempCount,uint8_t nextState,uint8_t regStatus);
				static uint8_t zeroCrossDetectSlow(uint16_t adcValue);
				
					
				static uint16_t adcRead(uint8_t adcChannel);				
				
				static uint8_t adcChannelRead;
                static uint8_t adcMxValue;				
				static uint8_t zeroCrossFlag = 0;
				static uint8_t rampUpStatus = 0;
                static uint8_t intStatReg = 0;
                static uint8_t evalBoardTest = 0;
                static uint8_t adcChannel;
                static uint8_t  commutationFlag = 0;
                static uint8_t pitMode = 0;
                static uint8_t tpm0EntryFlag = 0;
                static uint8_t zeroCrossFlagDet = 0;	
                
                // ---- experimental registers and functions start ---- //
                static uint8_t speedVaryDutyCycleSlow(uint8_t startDutyCycle,uint8_t finalDutyCycle,int tempCountSlow,uint8_t nextState,uint8_t regStatus);
                static uint8_t initIterationEntry = 0;    // -- first entry after stopping -- //
                static int tempCountFiltered = 0;         // -- after filtering of 10% -- //
                static void balanceCross(uint8_t finalDutyCycle,uint8_t nextState,int tempCountBanalce);
                // ---- experimental registers ends ---- //
                
                			
				
                static uint16_t adcDataObt;
                static uint16_t periodRamp = 0;
				static uint16_t tempCountValSaved = 0;
                
                volatile int tempCount;
                volatile uint8_t nextStateTemp;                
                static int commTimeAfterRampUp = 0;
                
                static uint8_t startDutyCycle = 0;   // added 03022015 //
                static uint8_t finalDutyCycle = 0;   // added 03022015 //
                static uint8_t regStatus = 0;        // added 03022015 //
                
                static zeroCrossTempFlag = 0;
                                
                static float commutationTimeValue = 0.0;                 
               
                volatile uint8_t commState;
                volatile uint8_t dutyCycle;
                volatile uint8_t zeroCrossFlagTemp;
                volatile tempCountIntermediate;
                volatile tempCountIntermediateSlow = 0;
                
                // -- pre processor # defines new -- //
                #define polePairs 6
                #define electricalStates 6
                #define polePairs 7
                #define commStates 6
                #define startupPeriod 10 // in msecs //
                #define rampIterationsFinal polePairs*commStates 
                #define rampIterationsInit rampIterationsFinal - 12
                #define dutyCycleBlindInit 90       // (10% duty cycle ) //
                #define dutyCycleBlindFinal 75		// (25% duty cycle ) //
                
                #define adcValMin 265
                #define adcValMax 340
                #define restartDelayCount 30
                #define startUpDutyCycleInit 0
                #define startUpDutyCycleFinal 15
                
                
                // --  rough declaration starts -- //            	

                	
               // --  rough declaration ends ---- //
                
//                 #define adcTestMode

// ----------------------------------------------------- //
// --------------- main loop --------------------------- //

                int main(void){	                

                    uint8_t zeroCrossFlag = 0;
                    uint8_t stallFlag = 0;
                    uint8_t restartDelay = 0;
                    
                    int rougher = 0;
                   
                    
                    
                    int roughCount = 0;
                    					
                    restartFlag = 0; 
                    
                    zeroCrossFlagTemp = 0;  
	                commutationTimerInit();
	                pwmInit();                       
	                gpioInit();   
	                pitInit();                                       
					commutationTimerInit();         // timer1 initialised - commutation //
	                adcInit();                                       
					interruptsInit();                        // configure interrupts //        
                    //GPIOB_PSOR |= 0x00000018;
                    for(rougher = 0;rougher <= 255; rougher++);
                   
                      
                    while(stallFlag != 1){  
	                    zeroCrossFlagTemp = 0;  
	                    commutationTimerInit();
	                    pwmInit();                       
	                    gpioInit();   
	                    pitInit();                                       
						commutationTimerInit();         // timer1 initialised - commutation //
	                    adcInit();                                       
						interruptsInit();                        // configure interrupts //                    
                    	//prePosition();
	                    do{
		                    commState = 0;
		                    zeroCrossFlag = blindStartUp();
	                    }              
	                    while(zeroCrossFlag == 0);
	                    zeroCrossFlag = 0;
	                    stallFlag = 0;
	                    while(1){
		                    stallFlag = runMode(commState);
		                    TPM0_SC &= ~(TPM_SC_CMOD_MASK);   // timer turned off //
		                    GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
							GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
							GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
							GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
							break;								
	                    }
	                    stallFlag = 0;
	                    zeroCrossFlag = 0;
	                    restartFlag = 1;
	                    for(restartDelay = 0;restartDelay <= restartDelayCount;restartDelay++){
	                    	for(roughCount = 0;roughCount<= 65000;roughCount++);
                    	}	                    
                    }
                }
                
// --------------- main done ---------------------------- //
// ------------------------------------------------------ //
	
// ------------------------------------------------------ //
// ----------------- run mode start --------------------- //
                static uint8_t runMode(uint8_t commState){  
	        		
	        		uint8_t nextState,ola;
	        		uint8_t pwmCount,stallDetect,initIteration,stallDetCount;
	        		uint8_t stallStat;
	        		
	        		uint16_t adcValPrev,adcValPres;
	        		
	        		uint16_t counterSecondary,counterPrime;
	        		ola = 0;
	        		stallDetect = 0;
	        		pwmCount = 0;
	        		nextState = commState;
	        		counterSecondary = 0;
	                counterPrime = 0;
	                
// 	                GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 					GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 					GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 					GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 					GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 					GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 					GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 					GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 					while(1);
        		    while(stallDetect == 0){
	        		    adcValPrev = 0;
	        		    adcValPres = 0;
		                stateChange(nextState,dutyCycle);
	                    commTimerFunc(tempCount);                   
	                    
	                    // --- here we add code to change duty cycle after a count is achieved --- //
	                    
	                    while(commutationFlag == 0){		                    		                    
			                if(tpm0EntryFlag == 1){
				                pwmCount++;
				                tpm0EntryFlag = 0;
				                if(nextState == 4){
					                adcChannel = 'V';
			                    	adcDataObt = adcRead(adcChannel);
					                if(initIteration == 1){			// -- to check if its the 1st entry. To filter the big spike at
					                								// -- the begining. -- //					                    												
						                initIteration = 0;
						                adcValPrev = adcDataObt;
					                }
					                else{
						                adcValPres = adcDataObt;	
// // 						             zeroCrossFlagDet = zeroCrossDetect(adcValPres);
// 						                stallStat = stallDetectFunction(adcValPres,adcValPrev);
// 										if(stallStat == 1){
// 										 stallStat = 0;
// 										 stallDetCount++;
// 										}										
										adcValPrev = adcValPres;  					                    
					                }
				                }				                
			                }
						}
						// --- stall detection done here --- //
// 						if((nextState == 4) && (pwmCount - stallDetCount <= 14)){
// 							stallDetect = 1;							
// 		                }		                        	
	                    commutationFlag = 0;
	                    pwmCount = 0;	
	                    stallDetCount = 0;
	                    nextState--;
			            if(nextState >=6) nextState = 5;
			            
			            
			            /*
			            if(counterSecondary == 3){
// 				            GPIOB_PTOR |= 0x00000018;
				            counterSecondary++;
				            startDutyCycle = 20;
				            finalDutyCycle = 50;	
				            regStatus = 75;	
				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);
				            
				            // -- write a function to 
				            nextState = commState;
				            tempCount = tempCountIntermediate;
				            ola = 75;
// 				            GPIOB_PTOR |= 0x00000018;

			            }
			            else if(counterSecondary == 5){
// 				            GPIOB_PSOR |= 0x00000018;
				            counterSecondary++;
				            startDutyCycle = 50;
				            finalDutyCycle = 80;	
				            regStatus = 75;
				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);
				            nextState = commState;
				            tempCount = tempCountIntermediate;
// 				            GPIOB_PCOR |= 0x00000018;
				            ola = 1;
			            }
// 			            else if(counterSecondary == 10){
// 				            dutyCycle = 20;
// 				            tempCount = 800;
// 			            }
			            else if(counterSecondary == 6){
				            ola = 2;
				            counterSecondary++;
				            startDutyCycle = 80;
				            finalDutyCycle = 50;	
				            regStatus = 75;
				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate + 25;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
			            else if(ola == 2){ // this shld now work I hope !!!!!!!!!! //
				            ola = 3;
				            counterSecondary++;
				            startDutyCycle = 50;
				            finalDutyCycle = 20;	
				            regStatus = 75;
				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate + 25;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
			            else if(counterSecondary == 8){
				            ola = 4;
				            counterSecondary++;
				            startDutyCycle = 20;
				            finalDutyCycle = 60;	
				            regStatus = 75;
				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate ;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
			            else if(ola == 4){
				            ola = 5;
				            counterSecondary++;
				            startDutyCycle = 60;
				            finalDutyCycle = 40;	
				            regStatus = 75;
				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            commState = nextStateTemp;
				            nextState = commState;
				            tempCount = tempCountIntermediate + 15;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
			            else if(ola == 5){
				            ola = 6;
				            counterSecondary++;
				            startDutyCycle = 40;
				            finalDutyCycle = 20;	
				            regStatus = 75;
				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);	
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate + 25;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
			            else if(ola == 6){
				            ola = 7;
				            counterSecondary++;
				            tempCount += 20;
				            startDutyCycle = 20;
				            finalDutyCycle = 50;	
				            regStatus = 75;
				            
				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);  
				            
// 				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
						
			            else if(ola == 7){
				            ola = 8;
				            counterSecondary++;
				            tempCount += 20;
				            startDutyCycle = 50;
				            finalDutyCycle = 75;	
				            regStatus = 75;
				            
				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);  
				            
// 				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
// 			            
// 			            
			            else if(ola == 8){
				            ola = 9;
				            counterSecondary++;
				            tempCount += 20;
				            startDutyCycle = 75;
				            finalDutyCycle = 100;	
				            regStatus = 75;
				            
				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);  
				            
// 				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
			            
			            
			            else if(ola == 9){
				            ola = 10;
				            counterSecondary++;
				            tempCount += 20;
				            startDutyCycle = 100;
				            finalDutyCycle = 65;	
				            regStatus = 75;
				            
// 				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);  
				            
				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
// 			            
// 			            
			            else if(ola == 10){
				            ola = 11;
				            counterSecondary++;
				            tempCount += 20;
				            startDutyCycle = 65;
				            finalDutyCycle = 35;	
				            regStatus = 75;
				            
// 				            zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);  
				            
				            zeroCrossFlagDet = speedVaryDutyCycleSlow(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);				
				                      
				            nextState = commState;
				            tempCount = tempCountIntermediate  ;
// 				            balanceCross(finalDutyCycle,nextState,tempCount);  
				            nextState = commState;
				            tempCount = tempCountIntermediate;
// 				            GPIOB_PCOR |= 0x00000018;
			            }
			            else{
				            counterPrime++;
				            if(counterPrime == 2000){
					            counterPrime = 0;
					            counterSecondary++;
				            }
			            }
			            */	        		    
        		    } 
        		    return(stallDetect);         		
        		}
	            
// ------------------------------------------------------ //
// ----------------- run mode ends ---------------------- //   
 
                
        		// --------- run mode finish ---------- // 
//         		static void runMode(uint8_t commState){  
// 	        		
// 	        		uint8_t nextState;
// 	        		  
// 	        		nextState = commState;   
// // 	        		dutyCycle = 10;   		
//         		    while(1){
// 		                stateChange(nextState,dutyCycle);
// 	                    commTimerFunc(tempCount);
// 	                    while(commutationFlag == 0){
// 						}		                        	
// 	                    commutationFlag = 0;
// 	                    nextState--;
// 			            if(nextState >=6) nextState = 5;	        		    
//         		    }          		
//         		}

        		
// ------------------------------------------------------ //
// ----------------- stall detection -------------------- //    
				static uint8_t stallDetectFunction(uint16_t adcValPres, uint16_t adcValPrev){
						               
	                uint8_t stallFlag = 0;	                
	                uint16_t adcDiffVal = 0;	                
	                if(adcValPres >= adcValPrev) adcDiffVal = adcValPres - adcValPrev;	                
	                else adcDiffVal = adcValPrev - adcValPres ;	                
	                if(adcDiffVal <= 4) stallFlag = 1;
	                return(stallFlag);
                }
// ----------------- stall detection -------------------- //    
// ------------------------------------------------------ //


// ------------------------------------------------------ //
// --------------- gpio init ---------------------------- //
// 				GPIO pins are initialised here.

                static void gpioInit(void){	                
	                
                    SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK;  // clk gated for PORTA
                    SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;  // clk gated for PORTB

                    PORTB_PCR6  &= ~(PORT_PCR_MUX_MASK);
                    PORTA_PCR5  &= ~(PORT_PCR_MUX_MASK);
                    PORTB_PCR10 &= ~(PORT_PCR_MUX_MASK);
                    //PORTB_PCR8  &= ~(PORT_PCR_MUX_MASK);         // eval board
                    PORTB_PCR6 |= PORT_PCR_MUX(1);
                    PORTA_PCR5 |= PORT_PCR_MUX(1);
                    PORTB_PCR10|= PORT_PCR_MUX(1);
                    //PORTB_PCR8 |= PORT_PCR_MUX(1);                 
   					// eval board
                    // pins are made output pins //
                    GPIOA_PDDR |=  0x00000020;
                    GPIOB_PDDR |=  0x00000040;
                    GPIOB_PDDR |=  0x00000400;
                    // diagnostic pins PTB3 and PTB4 //
                    PORTB_PCR3  &= ~(PORT_PCR_MUX_MASK);
                    PORTB_PCR3  |= PORT_PCR_MUX(1);
                    PORTB_PCR4  &= ~(PORT_PCR_MUX_MASK);
                    PORTB_PCR4  |= PORT_PCR_MUX(1);
                    GPIOB_PDDR |=  0x00000018;
                    // drive low - '0' on pins //
                    GPIOA_PCOR = GPIO_PCOR_PTCO_MASK;
                    GPIOB_PCOR = GPIO_PCOR_PTCO_MASK;
                    TPM0_SC |= TPM_SC_TOIE_MASK; // interrupt every 50uSecs(20KH) //
                    // diagnostic pins PTB3 and PTB4 //
                    GPIOB_PCOR |= 0x00000018;
                    // adc pins are made input //
                }
// --------------- gpio done --------------------------- //
// ----------------------------------------------------- //

// ----------------------------------------------------- //
// --------------- pwm init ---------------------------- //
                static void pwmInit(void){	                
	                
                    // PWM - PWM off time first //
                    //timer0 - PWM //
//                     SIM_SOPT2 |= SIM_SOPT2_TPMSRC(1);   // clk source for TPM0 and TPM1 as well
//                     SIM_SCGC6 |= SIM_SCGC6_TPM0_MASK;   // clk gated for TPM0
//                     SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK;  // clk gated for PORTA
//                     SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;  // clk gated for PORTB
//                     TPM0_SC |= 3;                   // 8 prescale factor
//                     SIM_SCGC5  |= 0x00000400;      // gating of clock for PORTA/B //
//                     // port settings //
//                     PORTB_PCR7         &= PORT_PCR_MUX_MASK;    // U1+
//                     PORTA_PCR6         &= PORT_PCR_MUX_MASK;    // V1+
//                     PORTB_PCR11        &= PORT_PCR_MUX_MASK;    // W1+
//                     PORTB_PCR7         |= PORT_PCR_MUX(2);   // U1+
//                     PORTA_PCR6         |= PORT_PCR_MUX(2);   // V1+
//                     PORTB_PCR11        |= PORT_PCR_MUX(2);   // W1+
//                     // added 040412014 //
//                     TPM0_SC &= ~(TPM_SC_CMOD_MASK);              // timer off //
//                     TPM0_CNT = 0;                                // counter value. NOTE :- when debug is aactive
//                     TPM0_CNT = 0;                                // counter value. NOTE :- when debug is aactive
//                     TPM0_CONF |= 192;                            // counter runs in debug mode as well //
//                     TPM0_MOD = 131;
//                     TPM0_SC |= TPM_SC_TOIE_MASK;                 // INTERRUPT ENABLED //
//                     // -- add ends -- //
//                     //timer0//
//                     TPM0_CNT = 0;                                // counter value. NOTE :- when debug is aactive
//                     TPM0_CONF |= 192;                            // counter runs in debug mode as well //
//                     
//
//  ------------------------------------------------------------------------------------------------------------  //      
//              
                    	// PWM - PWM on time first //
                    	// PWM //
                        //timer0 - PWM //
                        SIM_SOPT2 |= SIM_SOPT2_TPMSRC(1);   // clk source for TPM0 and TPM1 as well
                        SIM_SCGC6 |= SIM_SCGC6_TPM0_MASK;   // clk gated for TPM0
                        SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK;  // clk gated for PORTA
                        SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;  // clk gated for PORTB
                        TPM0_SC |= 3;                   // 8 prescale factor
                        SIM_SCGC5  |= 0x00000400;      // gating of clock for PORTA/B //
                        // port settings //
            			PORTB_PCR7         &= PORT_PCR_MUX_MASK;    // U1+
                        PORTA_PCR6         &= PORT_PCR_MUX_MASK;    // V1+
                        PORTB_PCR11 	   &= PORT_PCR_MUX_MASK;    // W1+
                        
                        PORTB_PCR7         |= PORT_PCR_MUX(2);   // U1+
                        PORTA_PCR6         |= PORT_PCR_MUX(2);   // V1+
                        PORTB_PCR11 	   |= PORT_PCR_MUX(2);   // W1+
                        
                        // added 040412014 //
                        TPM0_SC &= ~(TPM_SC_CMOD_MASK);                          // timer off //
                        TPM0_CNT = 0;                                // counter value. NOTE :- when debug is aactive
                        TPM0_CNT = 0;                                // counter value. NOTE :- when debug is aactive
                        TPM0_CONF |= 192;                            // counter runs in debug mode as well //                        
//                         TPM0_MOD = 262;                                       // period for 10kH //
                        TPM0_MOD = 131;                                       // period for 20kH //
                        TPM0_SC |= TPM_SC_TOIE_MASK;                 // INTERRUPT ENABLED //
                        // -- add ends -- //
                		//timer0//
                		TPM0_CNT = 0;                                // counter value. NOTE :- when debug is aactive
                		TPM0_CNT = 0;                                // counter value. NOTE :- when debug is aactive
                		TPM0_CONF |= 192;                            // counter runs in debug mode as well //
                        //Pins of PWM //
                        //--- alternate selection (all PWM are ALT2)---//
//                         TPM0_SC   |= TPM_SC_CPWMS_MASK;        // edge alligned mode //
                        TPM0_C0SC |= (TPM_CnSC_MSB_MASK | TPM_CnSC_ELSB_MASK); // PWM - edge ,...etc for centre alligned mode //
                        TPM0_C2SC |= (TPM_CnSC_MSB_MASK | TPM_CnSC_ELSB_MASK); // PWM - edge ,...etc for centre alligned mode //
                        TPM0_C4SC |= (TPM_CnSC_MSB_MASK | TPM_CnSC_ELSB_MASK); // PWM - edge ,...etc for centre alligned mode //

                }

// --------------- pwm done ---------------------------- //
// ----------------------------------------------------- //

// ----------------------------------------------------- //
// --------------- timer1 init ------------------------- //


                static void commutationTimerInit(void){ 
                    SIM_SCGC6 |= SIM_SCGC6_TPM1_MASK;   // clk gated for TPM1
//                     TPM1_SC |= 5;                       // 32 prescale factor  //
                    TPM1_SC |= 4;                       // 16 prescale factor  //
                    TPM1_SC &= ~(TPM_SC_CMOD_MASK);                   
      				// timer off //
                    TPM1_CNT = 0;                                // counter value. NOTE :- when debug is aactive
                    TPM1_CONF |= 192;                            // counter runs in debug mode as well //
                    TPM1_SC |= TPM_SC_TOIE_MASK;                 // INTERRUPT ENABLED //
                    TPM1_CNT = 0;                                // counter value. NOTE :- when debug is aactive                    
                    TPM1_CONF |= 192;                            // counter runs in debug mode as well //
                // TPM1_SC // used to turn on timer //

                }

// --------------- timer1 done ------------------------- //
// ----------------------------------------------------- //


// ------------------ adc related ------------------------- //
// -------------------------------------------------------- //

                static void adcInit(void){
                    uint8_t roughReg;
                    uint16_t varRough;
                    uint8_t calibStatus;

                    SIM_SCGC6 |= SIM_SCGC6_ADC0_MASK;
                    ADC0_SC1A = 0;
                    ADC0_SC1A |= 0x80;
                    adcConfig();
                    calibStatus = adcCalib();
                    if(calibStatus == 1){  // calibration successful //
                            varRough =(ADC0_CLP0+ADC0_CLP1+ADC0_CLP2+ADC0_CLP3+ADC0_CLP4+ADC0_CLPS)/2;
                            varRough |=0x8000;
                            ADC0_PG = varRough;
                    }
                    else while(1);
                    adcConfig();
                               
                }
                

                static void adcConfig(void){      	                
                    ADC0_CFG1 |= 8;     // 10 bit ADC mode //                    
                }


                static uint8_t adcCalib(void){  
                    uint16_t varRough;
                    uint8_t status;
                    ADC0_SC2 &=~ADC_SC2_ADTRG_MASK;
                    ADC0_SC3 &= ~ADC_SC3_CAL_MASK;
                    ADC0_SC3 |=  ADC_SC3_CAL_MASK;
                    while((ADC0_SC1A & 0x00000080) != 0x00000080);
                    // COCO flag is set //
                    ADC0_SC1A &= ~ADC_SC1_COCO_MASK;
                    status = 1;
                    return(status);
                }
                
                
                static uint16_t adcRead(uint8_t adcChannel){
                	switch(adcChannelRead){  	                	          	
                	    case 'W':
                	    PORTB_PCR1  &= ~PORT_PCR_MUX_MASK;   // --- W ad
                	    ADC0_SC1A = 0x05;
                	    break;                	
                	    case 'V':
                	    PORTB_PCR0 &= ~PORT_PCR_MUX_MASK;    // --- V adc
                	    ADC0_SC1A = 0x06;
                	    break;   
                	    case 'U':
                	    PORTA_PCR7  &= ~PORT_PCR_MUX_MASK;   // --- U adc
                	    ADC0_SC1A = 0x07;
                	    break;
                	    default:
                	    asm("nop");
                	    break;
                	}    
			        while((ADC0_SC1A & 0x00000080) != 0x00000080);
			        adcDataObt = ADC0_RA;
                    GPIOB_PTOR |= 0x0000010;
			        return(adcDataObt);
		        }

// --------------- adc done ---------------------------- //
// ----------------------------------------------------- //


// ----------------------------------------------------- //
// --------------- interrupts -------------------------- //

                static interruptsInit(void){	                
                    NVIC_ICPR = 1 << (22);   // pit timer //
                    NVIC_ISER = 1 << (22);
                    NVIC_ICPR = 1 << (18);   // timer 1 //
                    NVIC_ISER = 1 << (18);
                    NVIC_ICPR = 1 << (17);   // timer 0 //
                    NVIC_ISER = 1 << (17);
                }

// --------------- interrupts done --------------------- //
// ----------------------------------------------------- //
         
		
// ------------------------------------------------------ //
// --------------- prePosition algo --------------------- //
// 				The prePosition will make the motor to align a 
// 				fixed position. U1+ is PWM and V1- is made high.
//
        		static void prePosition(void){
	        		
	        		

        		}
// --------------- prePosition done -------------------- //
// ----------------------------------------------------- //		
                        	 



// ----------------------------------------------------- //
// --------------- ramp up ----------------------------- //

                // -- check flow in 19th Sept page -- //
                static uint8_t rampUp(uint8_t commState){
	                
                }
                       
// --------------- ramp up done ------------------------ //
// ----------------------------------------------------- //


// ------------------------------------------------------------ //
// --------------- blind start up ----------------------------- //
				 static uint8_t blindStartUp(void){   
	                             
					 uint8_t x;
	                // Note - Initially it is made to ramp up to about 25% //	 
	                // here-in we shall look at increasing the duty cycle and reducing tempCount as and when necessary //
					// stall detection could be done here //
					// adc related registers are cleared (custom variables) //					
					
                    uint8_t nextState = 0;
                    
                    
                    initIterationEntry = 1;    // -- first entry after stopping -- //
               		
                                        
                    nextState = commState;
                    tempCount = 6000;  
                    zeroCrossFlagDet = 0;   
					
                    
                    
                    // ---- ***** make a new function of duty cycle change ***** --- //
                    // ---- ***** added new code ***** --- //
                    
                    startDutyCycle = startUpDutyCycleInit;
                    finalDutyCycle = startUpDutyCycleFinal;
                    regStatus = 0;
                    zeroCrossFlagDet = speedVaryDutyCycle(startDutyCycle,finalDutyCycle,tempCount,nextState,regStatus);
//                     GPIOB_PCOR |= 0x00000018;                  
                    
	                // ---- ***** new function of duty cycle ends ***** --- //
	                // ---------------------------------------------------- //
	                
	                zeroCrossFlagTemp = 0;
	                tempCount = tempCountIntermediate;
	                return(zeroCrossFlagDet);
                }                
// --------------- blind start up ends ----------------------------- // 
// ----------------------------------------------------------------- //
                          	
    		    static uint8_t speedVaryDutyCycle(uint8_t startDutyCycle,uint8_t finalDutyCycle,int tempCount,uint8_t nextState,uint8_t regStatus){
	    		    
	    		    uint8_t zeroFlag = 0;
	    		    uint8_t slotCounts = 0;                    
                    uint8_t rpmStartup = 0;
                    uint8_t rough = 0;
                    uint8_t initIteration = 0;
                    uint8_t runTime = 0;
                    uint8_t flagger = 0;  
                    uint8_t pwmCount = 0;
                    uint8_t safetyCount = 0;
//                     uint8_t zeroFlag = 0;
                    uint8_t falsezeroCount = 0;
                    uint8_t failCycle = 0;
                    
                    
                    uint16_t adcValPrev = 0;
                    uint16_t adcValPres  = 0; 
                    
                    uint16_t adcInitVal = 0;
                    uint16_t adcFinalVal = 0;
                    
                    
                    uint16_t adcMisMatchCount = 0;
                    uint16_t adcDiff;
                    uint16_t adcDecrCount = 0;
                    uint16_t x = 0;
                                        
                    int commutationTimeValue = 0;
                    int tempCountInit;
                    int dutyCount;   
					int tempCountPrev = 0; 
					int tempCountFilteredRough = 0;
					                                    
                    float tempValue;
                    
                    failCycle = 0;	 
                    if(initIterationEntry == 1){
	                    tempCountFiltered = (30*tempCount)/100;
                    }   		    
//                     GPIOB_PCOR |= 0x00000018;                  
                    zeroFlag = 0;
                    dutyCycle = startDutyCycle; 
	                while(zeroFlag != 1){
	                    if(zeroFlag != 1){
		                    if(dutyCycle < finalDutyCycle){
			                    tempCountPrev = tempCount;
				            	tempCount -=1;
				            	dutyCycle++;
			                }	
			                else{    
				                tempCountPrev = tempCount;     
		                    	tempCount -= 1;  
	                    	}  
	                    	adcInitVal = 0;
	                    	adcFinalVal = 0;	                    	       
		                    // stall flag check done //	
		                    // adc custom variables clearing done next //		                    
		                    stateChange(nextState,dutyCycle);
                            commTimerFunc(tempCount);
                            initIteration = 1;
                            tempCountFilteredRough++;
                            failCycle = 0;
		                    while(commutationFlag == 0){
			                    
			                    if(tpm0EntryFlag == 1){
				                    
				                    tpm0EntryFlag = 0;
// 				                    while(failCycle == 0){
					                    
				                    	if(nextState == 4){
					                	    adcChannel = 'V';
			                        		adcDataObt = adcRead(adcChannel);			                        	
					                	    if(initIteration == 1){			// -- to check if its the 1st entry. To filter the big spike at // -- the begining. -- //					                    												
						            	        initIteration++;
					                	    }
					                	    else if(initIteration == 2){   
						            	        initIteration = 0;
						            	        adcInitVal = adcDataObt;
						            	        adcValPrev = adcDataObt; 
						            	        if(adcValPrev  >= 355)failCycle = 1;
						            	        	
					                	    }
					                	    else if(zeroFlag != 1){	
						            	        adcValPres = adcDataObt;
						            	        if(failCycle == 0)
						                    
						            	        	zeroFlag = zeroCrossDetect(adcValPres);						                    		
						            	        	if(adcValPrev >= adcValPres)falsezeroCount++;
						            	     		if(falsezeroCount >= 1)zeroFlag = 0; 
// 					                	 		}           
						            	        adcValPrev = adcValPres;		
			                        	
					                	    }
				                    	}
// 			                    	}//--
			                    }
		                    }
		                    
		                    // ---- what is this !!!!!!!!!!!! ---- //
		                    if((tempCountFilteredRough < tempCountFiltered) && (initIterationEntry == 1) &&(zeroFlag == 1))zeroFlag = 0; 
		                    falsezeroCount = 0;

                    
		                    adcFinalVal = adcValPrev;
		                    // --  now see if the init value is < final value. -- //
		                    if((adcInitVal >= adcFinalVal) || (adcInitVal >= 285)) zeroFlag = 0;
		                    	
		                    adcDecrCount = 0; 		                    
			                commutationFlag = 0;		                    
			                nextState--;
			                if(nextState >=6) nextState = 5;
			                zeroCrossTempFlag = 0;
		                }
	                }
	                GPIOB_PTOR |= 0x00000018;	
	                initIterationEntry = 2;
	    		    tempCountIntermediate = tempCount;
	    			zeroCrossFlagTemp = 0;
					commState = nextState;
					

	                return(zeroFlag);
	    		    
	    		    
    		    }	
				
    		    
    		    
    		    
    		    
    		    static uint8_t speedVaryDutyCycleSlow(uint8_t startDutyCycle,uint8_t finalDutyCycle,int tempCountSlow,uint8_t nextState,uint8_t regStatus){
	    		    
	    		    uint8_t zeroFlag = 0;
	    		    uint8_t slotCounts = 0;                    
                    uint8_t rpmStartup = 0;
                    uint8_t rough = 0;
                    uint8_t initIteration = 0;
                    uint8_t runTime = 0;
                    uint8_t flagger = 0;  
                    uint8_t pwmCount = 0;
                    uint8_t safetyCount = 0;
                    uint8_t countSlowPause = 0;
                    uint8_t startLookCross = 0;
                    uint8_t falsezeroCount = 0;
                    
                    uint16_t adcValPrev = 0;
                    uint16_t adcValPres  = 0; 
                    
                    uint16_t adcInitVal = 0;
                    uint16_t adcFinalVal = 0;
                    
                    
                    uint16_t adcMisMatchCount = 0;
                    uint16_t adcDiff;
                    uint16_t adcDecrCount = 0;
                    uint16_t x = 0;
                                        
                    int commutationTimeValue = 0;
                    int tempCountInit;
                    int dutyCount;   
					int tempCountPrev = 0; 
					int tempCountFilteredRough = 0;
					int tempCountFilter;                                   
					
                    float tempValue;   
                    
                    
                    tempCountFilter = (tempCountSlow * 3)/100;
                    tempCountFilter += tempCountSlow;
                    zeroFlag = 0;
                    dutyCycle = startDutyCycle; 
	                while(zeroFlag != 1){
	                    if(zeroFlag != 1){
		                    	if(dutyCycle > finalDutyCycle){
			                	    tempCountPrev = tempCountSlow;
				            		tempCountSlow +=1;
				            		dutyCycle--;
			                	}	
			                	else{  
				                	startLookCross = 1;  
				            	    tempCountPrev = tempCountSlow;     
		                    		tempCountSlow += 1;  
	                    		} 
//                     		} 
                    		countSlowPause++;
	                    	adcInitVal = 0;
	                    	adcFinalVal = 0;	                    	       
		                    // stall flag check done //	
		                    // adc custom variables clearing done next //		                    
		                    stateChange(nextState,dutyCycle);
                            commTimerFunc(tempCountSlow);
                            initIteration = 1;
                            tempCountFilteredRough++;
		                    while(commutationFlag == 0){			                    
			                    if(tpm0EntryFlag == 1){
				                    tpm0EntryFlag = 0;
				                    if(nextState == 4){
					                    adcChannel = 'V';
			                        	adcDataObt = adcRead(adcChannel);			                        	
					                    if(initIteration == 1){			// -- to check if its the 1st entry. To filter the big spike at // -- the begining. -- //					                    												
						                    initIteration++;
					                    }
					                    else if(initIteration == 2){   
						                    initIteration = 0;
						                    adcInitVal = adcDataObt;
						                    adcValPrev = adcDataObt; 			                    
					                    }
					                    else if(zeroFlag != 1){	
						                    adcValPres = adcDataObt;
						                    if(startLookCross == 1){
							                    if(tempCountSlow > tempCountFilter)					                    
						                    		zeroFlag = zeroCrossDetectSlow(adcValPres);	
					                    	}				                    		
						                    if(adcValPrev >= adcValPres)falsezeroCount++;
						                 	if(falsezeroCount >= 1)zeroFlag = 0; 
						                    adcValPrev = adcValPres;
					                    }
				                    }
			                    }
		                    } 
		                    falsezeroCount = 0;
		                    adcFinalVal = adcValPrev;
		                    // --  now see if the init value is < final value. -- //
		                    if((adcInitVal >= adcFinalVal) || (adcInitVal >= 285)) zeroFlag = 0;		                    	
		                    adcDecrCount = 0; 		                    
			                commutationFlag = 0;		                    
			                nextState--;
			                if(nextState >=6) nextState = 5;
			                zeroCrossTempFlag = 0;
		                }
	                }
	                
	                
// 	                if(finalDutyCycle == 40){
// 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 					}
	                GPIOB_PTOR |= 0x00000018;
	                nextStateTemp = nextState;
	    		    tempCountIntermediate = tempCountSlow + 15;	    		    
	    			zeroCrossFlagTemp = 0;
					commState = nextStateTemp;
	                return(zeroFlag);
	    		    
	    		    
    		    }	
// ----------------------------------------------------------------- //		
// --------------- '0' cross detection ----------------------------- //		
                static uint8_t zeroCrossDetect(uint16_t adcValue){
	                
	                //	arguments used - tempCount increment/decrement value(e.g - 5),
					//					 it shld know if increment or decrement time value(tempCount) //
					//					 zeroCrossStatReg ---- 	 
	               	 	
	                if((adcValue >= adcValMin) && (adcValue <= adcValMax)){			                		                                    			                        			
						// '0' cross detected //
// 						GPIOB_PSOR |= 0x00000018;
						tempCountValSaved = TPM1_CNT;
						if((tempCountValSaved >= ((tempCount/2) - 120)) && (tempCountValSaved <= ((tempCount/2)+ 120))){							
						    zeroCrossFlagTemp  = 1;							     
//   						    commutationFlag = 1; 
						}	       
					}
					return(zeroCrossFlagTemp);
										
                }
                
                
                static uint8_t zeroCrossDetectSlow(uint16_t adcValue){
	                
	                //	arguments used - tempCount increment/decrement value(e.g - 5),
					//					 it shld know if increment or decrement time value(tempCount) //
					//					 zeroCrossStatReg ---- 	 
	               	 	
	                if((adcValue >= adcValMin) && (adcValue <= adcValMax)){			                		                                    			                        			
						// '0' cross detected //
// 						GPIOB_PSOR |= 0x00000018;
						tempCountValSaved = TPM1_CNT;
						if((tempCountValSaved >= ((tempCount/2) - 150)) && (tempCountValSaved <= ((tempCount/2)+ 150))){							
						    zeroCrossFlagTemp  = 1;		
// 						    GPIOB_PSOR |= 0x00000018;   					     
//   						    commutationFlag = 1; 
						}	
// 						GPIOB_PCOR |= 0x00000018;       
					}
					return(zeroCrossFlagTemp);
										
                }
                
                
                
                static void balanceCross(uint8_t finalDutyCycle,uint8_t nextState,int tempCountBanalce){
	                
	                
	                uint8_t zeroFlag = 0;
	    		    uint8_t slotCounts = 0;                    
                    uint8_t rpmStartup = 0;
                    uint8_t rough = 0;
                    uint8_t initIteration = 0;
                    uint8_t runTime = 0;
                    uint8_t flagger = 0;  
                    uint8_t pwmCount = 0;
                    uint8_t safetyCount = 0;
                    uint8_t countSlowPause = 0;
                    uint8_t startLookCross = 0;
//                     uint8_t zeroFlag = 0;
                    uint8_t falsezeroCount = 0;
                    
                    uint16_t adcValPrev = 0;
                    uint16_t adcValPres  = 0; 
                    
                    uint16_t adcInitVal = 0;
                    uint16_t adcFinalVal = 0;
                    
                    
                    uint16_t adcMisMatchCount = 0;
                    uint16_t adcDiff;
                    uint16_t adcDecrCount = 0;
                    uint16_t x = 0;
                                        
                    int commutationTimeValue = 0;
                    int tempCountInit;
                    int dutyCount;   
					int tempCountPrev = 0; 
					int tempCountFilteredRough = 0;
					int tempCountSlow = 0;
	                GPIOB_PCOR |= 0x00000018;
			        GPIOB_PSOR |= 0x00000018;	   		     	
					for(tempCountSlow = tempCountBanalce;tempCountSlow <= tempCountBanalce + 20;tempCountSlow+=5){
// 	                	adcInitVal = 0;
// 	                	adcFinalVal = 0;	                    	       
		            	// stall flag check done //	
		            	// adc custom variables clearing done next //		                    
		            	stateChange(nextState,dutyCycle);
                    	commTimerFunc(tempCountSlow);
                    	initIteration = 1;
                    	tempCountFilteredRough++;
//                     	GPIOB_PTOR |= 0x00000018;
		            	while(commutationFlag == 0);
		            	
		            	// --  now see if the init value is < final value. -- //		                    	              
			        	commutationFlag = 0;		                    
			        	nextState--;
			        	if(nextState >=6) nextState = 5;			                	
		            }
		            
		            
// 		            	GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
		            tempCountIntermediate = tempCountSlow ;	
// 		            GPIOB_PCOR |= 0x00000018;
		            nextStateTemp = nextState;
// 		            commState = nextState;	
	            } 
// 	                	}
	                					
// // 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// // 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// // 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// // 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// // 						GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
// // 						GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
// // 						GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
// // 						GPIOB_PCOR |= 0x00000040;  // T2 - cleared //	
// 	    		    	
// 	                
// 	                
//                 	}
// --------------------- '0' cross ends ---------------------------- //
// ----------------------------------------------------------------- //
                
// ----------------------------------------------------------------- //
// ------------------------ stateChange ---------------------------- //			
				
                static void stateChange(uint8_t nextState,uint8_t dutyCycle){	                
                    uint8_t state,channelNext;
                    uint16_t rough = 0;
                    state = nextState;
                    rough = dutyCycle;
                    rough = (rough * 131)/100;
                    dutyCycle = rough;
					
                    switch(state){
	                    case 0:
                        	// T1 and T4  -- W float //
                        	GPIOB_PCOR |= 0x00000440; // T2 and T6 - cleared //
                        	GPIOA_PSOR |= 0x00000020; // T4 - set //
                        	channelNext = 2;
                        	adcChannelRead = 'W';
                        	setPWMDutyCycle(dutyCycle,channelNext);
                        	break;
                        	
                        	case 1:
                        	// T1 and T6 -- V float //
                        	GPIOB_PSOR |= 0x00000400; // T6 - set // 
                        	channelNext = 2;
                        	adcChannelRead = 'V';
                        	setPWMDutyCycle(dutyCycle,channelNext);
                        	break;
                        	
                        	case 2:
                        	// T3 and T6 --  U float //
                        	GPIOB_PCOR |= 0x00000040;  // T2 - cleared //
                        	GPIOB_PSOR |= 0x00000400;  // T6 - set //  
                        	channelNext = 4;
                        	adcChannelRead = 'U';
                        	setPWMDutyCycle(dutyCycle,channelNext);
                        	//state++;
                        	break;
                        	
                        	case 3:
                        	// T3 and T2 -- W float //
                        	GPIOA_PCOR |= 0x00000020;  // T4 - cleared // 
                        	channelNext = 4;
                        	adcChannelRead = 'W';
                        	setPWMDutyCycle(dutyCycle,channelNext);
                        	//GPIOB_PCOR |=  0x00000018;
                        	//state++;
                        	break;
                        	
                        	case 4:
                        	// T5 and T2 -- U float //
                        	GPIOA_PCOR |= 0x00000020;  // T4 - cleared //
                        	GPIOB_PSOR |= 0x00000040;  // T2 - set //  
                        	channelNext = 0;
                        	adcChannelRead = 'V';
                        	setPWMDutyCycle(dutyCycle,channelNext);
                        	//state++;
                        	break;
                        	
                        	case 5:
                        	// T5 and T4 -- U float //
                        	GPIOB_PCOR |= 0x00000400;  // T6 - cleared //
                        	channelNext = 0;
                        	adcChannelRead = 'U';
                        	setPWMDutyCycle(dutyCycle,channelNext);
                        	break;  
                    }
                }

// ------------------------ stateChange done ---------------------------- //
// ---------------------------------------------------------------------- //

// ---------------------------------------------------------------------- //
// ------------------------ commutation timer --------------------------- //

                static void commTimerFunc(uint16_t commutationTimeValue){ 
                    TPM1_MOD = commutationTimeValue;  // period loaded //
                    TPM1_SC |= 8;                                    
					// timer started //
                }

// ------------------------ commutation timer done ---------------------- //
// ---------------------------------------------------------------------- //                


// -------------------------------------------------------------- //
// ------------------------ PWM timer --------------------------- //

                static void setPWMDutyCycle(uint16_t dutyCycle,uint8_t channel){
                    float roughVal;
                    uint8_t xRough = 0; // comment later //
                    uint8_t result0A = 0; // comment later //

                    TPM0_SC &= ~(TPM_SC_CMOD_MASK);                   
                    TPM0_CNT = 0;                            // counter value. NOTE :- when debug is aactive
                    TPM0_CONF |= 192;                        // counter runs in debug mode as well //
                    switch(channel){
                    // '-' or low drivers are pulled high without PWMs as they don't make a difference //
                    // pwm off time sent initially //

//                     	case 0:
//                     	TPM0_C2SC &= ~(TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //
//                     	TPM0_C4SC &= ~(TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //
//                     	TPM0_C0SC |= (TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //  
//                     	TPM0_C0V = dutyCycle;   
//                     	break;
//                     	    
//                     	case 2:
//                     	TPM0_C0SC &= ~(TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //
//                     	TPM0_C4SC &= ~(TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //
//                     	TPM0_C2SC |= (TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //                    
//                     	TPM0_C2V = dutyCycle;       // U1+ - ON //
//                     	break;
//                     	
//                     	case 4:
//                     	TPM0_C0SC &= ~(TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				SC |= (TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //
//                     	TPM0_C2SC &= ~(TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //
//                     	TPM0_C4SC |= (TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK); // PWM - edge ,...etc for centre alligned mode //                    
//                     	TPM0_C4V = dutyCycle;             // W1+ turned ON // uncomment
//                     	break;

						// pwm on time sent initially //
						case 0:
                                TPM0_C0V = dutyCycle;                    // U1+ turned ON // uncomment
                                TPM0_C2V = 0;                    // V1+ turned OFF // uncomment
                                TPM0_C4V = 0;                    // W1+ turned OFF // uncomment

                        break;
                        case 2:
                                TPM0_C2V = dutyCycle;                    // V1+ turned ON // uncomment
                                TPM0_C0V = 0;                    // U1+ turned OFF // uncomment
                                TPM0_C4V = 0;                    // W1+ turned OFF // uncomment
                        break;
                        case 4:
                                TPM0_C4V = dutyCycle;                    // W1+ turned ON // uncomment
                                TPM0_C2V = 0;                    // V1+ turned OFF // uncomment
                                TPM0_C0V = 0;                    // U1+ turned OFF // uncomment
                        break;

                    }
                    TPM0_SC |= TPM_SC_CMOD(1); // timer started //
                }

// --------------- commutation done -------------------- //
// ----------------------------------------------------- //


// ----------------------------------------------------- //
// --------------- UART config and display ------------- //

//                 static uint8_t uartConfig(void){     
//                     SIM_SOPT2 |= 0x01000000;      // clock sel for the timer0 //
//                     SIM_SCGC5 |= 0x00000400;          // gating of clock //
//                     SIM_SCGC6 |= 0x01000000;      // gating of timer //
//                     SIM_SOPT2 |= SIM_SOPT2_UART0SRC(1);
//                     SIM_SCGC4 |= SIM_SCGC4_UART0_MASK;
//                     SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;
// 
//                     PORTB_PCR3 = 0x00000300;
//                     
//                     UART0_C2 &= ~(UART0_C2_TE_MASK| UART0_C2_RE_MASK);
// 
//                     PORTB_PCR8 |= 0x00000100;    // alternative pin mux selection //
//                     GPIOB_PDDR |= 0x00000100;    // pin direction //
// 
// 
//                     UART0_BDH = 0;
//                     UART0_BDL = 6;
// 
//                     UART0_C4 = 0x0F;
// 
//                     UART0_C1 = 0x00;
//                     UART0_C3 = 0x00;
//                     UART0_MA1 = 0x00;
//                     UART0_MA1 = 0x00;
//                     UART0_S1 |= 0x1F;
//                     UART0_S2 |= 0xC0;
//                     UART0_C2 |= UART0_C2_TE_MASK| UART0_C2_RE_MASK;
//                 }
// 
// 
// 
// 				static uint8_t uartDisp(uint8_t dispData){  					
// 					uint8_t rough;
// 					uint8_t i;
// 			
// 					rough = 0xf0;
// 					rough &= dispData;
// 					rough = (rough >>4);
// 					if(rough<=0x09){
// 							rough += 0x30;
// 					}
// 					else{
// 						rough -= 0x0a;
// 						rough += 0x61;
// 					}
// 					UART0_D = rough;
// 					while((UART0_S1 & 0x40) != 0x40);
// 					//for(i =0;i<=200;i++);
// 					rough = 0x0F;
// 					rough &= dispData;			
// 					if(rough<0x09){
// 						rough += 0x30;
// 					}
// 					else{
// 						rough -= 0x0a;
// 						rough += 0x61;
// 					}
// 					UART0_D = rough;
// 					while((UART0_S1 & 0x40) != 0x40);
// 					UART0_D = 0x20;
// 					while((UART0_S1 & 0x40) != 0x40);
// 				}

// ----------------------------------------------------- //
// --------------- PIT init ---------------------------- //

        		static void pitInit(void){ 
        		    SIM_SCGC6 |= 0x00800000;
        		    SIM_CLKDIV1 = 0;
        		    SIM_SCGC5 |= 0x00000400;
        		    PORTB_PCR8 |= 0x00000100;
        		    GPIOB_PDDR |= 0x00000100;    // pin direction //
        		    NVIC_ICPR = 1 << (22);
        		    NVIC_ISER = 1 << (22);        		
        		    PIT_MCR = 0x00;
        		    PIT_TCTRL1 = PIT_TCTRL_TIE_MASK; // enable Timer 1 interrupts              		
        		}

            	static void pitStart(int pitTout){
            	    // pitTout is always in micro seconds //
            	    int pitValue;
            	    pitValue = (20.97*pitTout) - 1;
            	    PIT_LDVAL1 = pitValue; // setup timer 1 for 256000 cycles
            	    PIT_TCTRL1 |= PIT_TCTRL_TEN_MASK; // start Timer 1            	
            	}



// --------------- PIT init done ----------------------- //
// ----------------------------------------------------- //



// ------------------- &&&&&&&&&& ---------------------- //
// -------- ISR (Interrupt service routine) -------------//
// ------------------- &&&&&&&&&& ---------------------- //

        		void PIT_IRQHandler(){

    			}


    			void FTM0_IRQHandler(void){   
	    			
	    			tpm0EntryFlag = 1;
    			    uint8_t rough = 0;
    			    TPM0_SC &= ~TPM_SC_CMOD_MASK;    			    
    			    if((TPM0_SC & TPM_SC_TOF_MASK) == TPM_SC_TOF_MASK){
    			            TPM0_SC |= TPM_SC_TOF_MASK;
    			    } 
    			    TPM0_SC |= 8;
    			}



    			void FTM1_IRQHandler(void){	    			
    			    TPM1_SC &= ~TPM_SC_CMOD_MASK;
    			    commutationFlag = 1;
    			    if((TPM1_SC & TPM_SC_TOF_MASK) == TPM_SC_TOF_MASK){
				        TPM1_SC |= TPM_SC_TOF_MASK;
    			    }
    			}



// ------------------- &&&&&&&&&& ---------------------- //
// -------- ISR (Interrupt service routine) -------------//
// ------------------- &&&&&&&&&& ---------------------- //
